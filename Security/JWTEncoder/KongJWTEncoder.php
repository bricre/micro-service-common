<?php


namespace Bricre\MicroServiceCommonBundle\Security\JWTEncoder;

use Exception;
use JWT\Authentication\JWT;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class KongJWTEncoder implements JWTEncoderInterface
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $kong;

    /**
     * KongJWTEncoder constructor.
     * @param HttpClientInterface $kong
     */
    public function __construct(HttpClientInterface $kong)
    {
        $this->kong = $kong;
    }

    /**
     * {@inheritdoc}
     */
    public function encode(array $data)
    {
        if (!key_exists('username', $data)) {
            throw new JWTEncodeFailureException(
                JWTEncodeFailureException::INVALID_CONFIG,
                "'username' field was not set in the payload"
            );
        }

        try {
            $credential = $this->getJWTCredential($data);
            // https://docs.konghq.com/hub/kong-inc/jwt/#craft-a-jwt-with-a-secret-hs256
            $data['iss'] = $credential['key'];

            return JWT::encode($data, $credential['secret']);
        } catch (Exception $e) {
            throw new JWTEncodeFailureException(
                JWTEncodeFailureException::INVALID_CONFIG,
                'An error occurred while trying to encode the JWT token.',
                $e
            );
        }
    }

    private function getJWTCredential(array $data)
    {
        $consumer = $this->kong->request(
            'PUT',
            '/consumers/'.$data['username'],
            [
                'json' => ['username' => $data['username']],
            ]
        )->toArray();


        $jwtCredential = $this->kong->request('GET', '/consumers/'.$data['username'].'/jwt')->toArray();
        if (0 == count($jwtCredential['data'])) {
            $jwtCredential['data'][] = $this->kong->request('POST', '/consumers/'.$data['username'].'/jwt')->toArray();
        }

        return $jwtCredential['data'][0];
    }

    /**
     * {@inheritdoc}
     */
    public function decode($token)
    {
        try {
            return (array)JWT::decode($token, null, false);
        } catch (Exception $e) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::INVALID_TOKEN, 'Invalid JWT Token', $e);
        }
    }
}