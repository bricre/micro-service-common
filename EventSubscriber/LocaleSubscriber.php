<?php

namespace Bricre\MicroServiceCommonBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
    protected string $currentLocale;


    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST  => [['onKernelRequest', 201]],
            KernelEvents::RESPONSE => ['setContentLanguage']
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        if ($request->headers->has("X-LOCALE")) {
            $locale = $request->headers->get('X-LOCALE');
            $request->setLocale($locale);
        } else {
            $request->setLocale('en');
        }

        // Set currentLocale for response
        $this->currentLocale = $request->getLocale();
    }

    public function setContentLanguage(ResponseEvent $event): \Symfony\Component\HttpFoundation\Response
    {
        $response = $event->getResponse();
        $response->headers->add(['Content-Language' => $this->currentLocale]);

        return $response;
    }

}
