Symfony bundle - micro-service-common

This is a Symfony bundle contains some commonly used components for Bricre micro services.

# Installation

```shell script
composer require bricre/micro-service-common
```

# Security

[LexikJWTAuthenticationBundle](https://github.com/lexik/LexikJWTAuthenticationBundle) is used to provide JWT based security guard.

```yaml
# security.yaml
security:
# https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
  providers:
    # used to reload user from session & other features (e.g. switch_user)
    app_user_provider:
      entity:
        class: App\Entity\User
        property: username

  firewalls:
    main:
      anonymous: true
      lazy: true
      provider: app_user_provider
      stateless: true
      json_login:
        check_path: /api/user/login
        success_handler: lexik_jwt_authentication.handler.authentication_success
        failure_handler: lexik_jwt_authentication.handler.authentication_failure
      guard:
        authenticators:
          - lexik_jwt_authentication.jwt_token_authenticator
```

To use this bundle, you will have to have [Kong](https://konghq.com/kong) available as APIGateway. Internally it calls Kong's 
admin API to create [Consumer](https://docs.konghq.com/2.1.x/admin-api/#consumer-object) and retrieves JWT credentials.

Make sure in your .env to have SERVICE_KONG environment variable pointing to the Kong server

```dotenv
SERVICE_KONG=kong
```

## Security work flow

1. `json_login` will use `app_user_provider` to validate username and password
1. Once login credential accepted,  `lexik_jwt_authentication.handler.authentication_success` will return a JWT token, 
    where `Bricre\MicroServiceCommonBundle\EventListener\JWTCreatedListener` would inject the current User information
    into the token.
1. User then use `Authentication: Bearer xxxxxxxx` header to make request
1. Since the User info has already existed in the token (JWT payload),  `lexik_jwt_authentication.jwt_token_authenticator`
    would automatically decode the token and authenticate the user.