<?php


namespace Bricre\MicroServiceCommonBundle;


use Bricre\MicroServiceCommonBundle\DependencyInjection\BricreMicroServiceCommonExtension;
use Bricre\MicroServiceCommonBundle\DependencyInjection\Compiler\MicroServiceCommonCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MicroServiceCommonBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null == $this->extension) {
            $this->extension = new BricreMicroServiceCommonExtension();
        }

        return $this->extension;
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new MicroServiceCommonCompilerPass());
    }
}